<?php
/**
 * Description of DefaultPage
 *
 * @author dmitr
 */
class pageclients_models_elements_Staff
{
    protected $parser;
    public $bActive = false;
    public $bPublished = false;
    public $bRandom = false;
    
    public function __construct($parser)
    {
        $this->parser = $parser;
    }

    function getStaffList($iStaffId = 0, $sFieldsForSelect = '', $bActive = false, $bPublished = false, $iListLimit = 0, $bRandom = false, $aElaborationList = []) 
    {

        $oSqlObject = new ui_clasess_Sql();
        
        if($sFieldsForSelect != '')
        {
            $aFieldsForSelect = explode(',', $sFieldsForSelect);
            foreach($aFieldsForSelect as $sFieldForSelect)
            {
                $oSqlObject->select($sFieldForSelect);
            }
        }
        else
        {
            $oSqlObject->select('*');
        }
        
        $oSqlObject->from('staff');
        
        if($bActive)
        {
           $oSqlObject->where('active=1');
        }
        if($bPublished)
        {
            $oSqlObject->where('published=1');
        }
        if($iStaffId != 0)
        {
            $oSqlObject->where('id="'.$iStaffId.'"');
        }
        if($aElaborationList != [])
        {
            $sElaborationList = implode(',', $aElaborationList);
            $oSqlObject->where('id NOT IN("'.$sElaborationList.'")');
        }
        
        $sSql = $oSqlObject->getSql();
        
        $old =  $this->parser->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        if(($iListLimit != 0) && !$bRandom)
        {
            $oResult = $this->parser->conn->SelectLimit($sSql, $iListLimit);
        }
        else 
        {
            $oResult = $this->parser->conn->SelectLimit($sSql);
        }
        $this->parser->conn->SetFetchMode($old);
        
        while($row = $oResult->FetchRow())
        {
            $row['iframeurl'] = '/staff/'.$row['id'];
            $aResult[] = $row;
        }
        
        if($bRandom)
        {
            if($iListLimit == 0)
            {
                $iListLimit = 1;
            }
            if(count($aResult) < $iListLimit)
            {
                $iListLimit = count($aResult);
            }

            $aRandomKeys = array_rand($aResult, $iListLimit);
            
            $aTempResult = array();
            for($i = 0; $i < $iListLimit; $i++)
            {
                $aTempResult[] = $aResult[$aRandomKeys[$i]];
            }
            $aResult = $aTempResult;
        }
        
        if(empty($aResult))
        {
            return array();
        }
        
        return $aResult;
    }
    
    function getStaffListWithPosition($iStaffId = 0, $bActive = false, $bPublished = false, $iListLimit = 0, $bRandom = false, $aElaborationList = []) 
    {

        $oSqlObject = new ui_clasess_Sql();
        
        
        $oSqlObject->select('*');
        $oSqlObject->select('stf.id');
        
        $oSqlObject->from('staff stf');
        $oSqlObject->from('staffposition stfp');
        $oSqlObject->from('position pos');
        
        if($bActive)
        {
           $oSqlObject->where('stf.active=1');
        }
        if($bPublished)
        {
            $oSqlObject->where('stf.published=1');
        }
        if($iStaffId != 0)
        {
            $oSqlObject->where('stf.id="'.$iStaffId.'"');
        }
        if($aElaborationList != [])
        {
            $sElaborationList = implode(',', $aElaborationList);
            $oSqlObject->where('stf.id NOT IN("'.$sElaborationList.'")');
        }
        
        $oSqlObject->where('stf.id=stfp.staffid');
        $oSqlObject->where('stfp.positionid=pos.id');
        
        $sSql = $oSqlObject->getSql();
        
        $old =  $this->parser->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        if(($iListLimit != 0) && !$bRandom)
        {
            $oResult = $this->parser->conn->SelectLimit($sSql, $iListLimit);
        }
        else 
        {
            $oResult = $this->parser->conn->SelectLimit($sSql);
        }
        $this->parser->conn->SetFetchMode($old);
        
        $aResult = array();
        while($oRow = $oResult->FetchRow())
        {
            if(!array_key_exists($oRow['id'], $aResult))
            {
                foreach($oRow as $sRowElementKey => $sRowElementValue)
                {
                    if($sRowElementKey != 'title')
                    {
                        $aResult[$oRow['id']][$sRowElementKey] = $sRowElementValue;
                        $aResult[$oRow['id']]['iframeurl'] = '/staff/'.$oRow['id'];
                    }
                }
                $aResult[$oRow['id']]['positions'] = [['title' => $oRow['title'], 'main' => 1]];
            }
            else
            {
                array_push($aResult[$oRow['id']]['positions'], ['title' => $oRow['title'], 'main' => 0]);
            }
        }

        if($bRandom)
        {
            if($iListLimit == 0)
            {
                $iListLimit = 1;
            }
            if(count($aResult) < $iListLimit)
            {
                $iListLimit = count($aResult);
            }

            $aRandomKeys = array_rand($aResult, $iListLimit);
            
            $aTempResult = array();
            for($i = 0; $i < $iListLimit; $i++)
            {
                $aTempResult[] = $aResult[$aRandomKeys[$i]];
            }
            $aResult = $aTempResult;
        }
        
        if(empty($aResult))
        {
            return array();
        }
        
        return $aResult;
    }
    
    function generateShortUrl($bRewrite = false, $iStaffId = 0, $bActive = false, $bPublished = false) 
    {
        $oSqlObject = new ui_clasess_Sql();

        $oSqlObject->select('*');

        $oSqlObject->from('staff');

        if($bActive)
        {
           $oSqlObject->where('active=1');
        }
        if($bPublished)
        {
            $oSqlObject->where('published=1');
        }
        if($iStaffId != 0)
        {
            $oSqlObject->where('id="'.$iStaffId.'"');
        }

        $sSql = $oSqlObject->getSql();

        $old =  $this->parser->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $oResult = $this->parser->conn->SelectLimit($sSql);
        $this->parser->conn->SetFetchMode($old);

        while($row = $oResult->FetchRow())
        {
            if($bRewrite || ($row['shorturl'] == ''))
            {
                $sTransName = strtolower($this->noDiacritics($row['fullname']));
                $oCoincidence = $this->parser->conn->SelectLimit('SELECT id FROM staff WHERE shorturl="'.$sTransName.'"');
                if($oCoincidence)
                {
                    $sSqlForUpdate = 'UPDATE staff SET shorturl="'.$sTransName.'_staff" WHERE id='.$row['id'];
                }
                else
                {
                    $sSqlForUpdate = 'UPDATE staff SET shorturl="'.$sTransName.'" WHERE id='.$row['id'];
                }
                $oResultExe = $this->parser->conn->Execute($sSqlForUpdate);
            }
        }
        
        $aResult = ['statuscode' => 200, 'mess' => 'success'];
                
        return $aResult; 
    }
    
    function noDiacritics($string)
    {
        if(!$string)
            return '';
        
        $cyrylicFrom = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё',  'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц',  'Ч',  'Ш',  'Щ', 'Ъ', 'Ы', 'Ь', 'Э',  'Ю',  'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё',  'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц',  'ч',  'ш',  'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я');
        $cyrylicTo   = array('A', 'B', 'V', 'G', 'D', 'E', 'E', 'ZH', 'Z', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', 'CH', 'SH', 'SH',  '', 'Y',  '', 'E', 'JU', 'JA', 'a', 'b', 'v', 'g', 'd', 'e', 'o', 'zh', 'z', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sh',  '', 'y',  '', 'e', 'ju', 'ja'); 

        $from = array("Á", "À", "Â", "Ä", "Ă", "Ā", "Ã", "Å", "Ą", "Æ", "Ć", "Ċ", "Ĉ", "Č", "Ç", "Ď", "Đ", "Ð", "É", "È", "Ė", "Ê", "Ë", "Ě", "Ē", "Ę", "Ə", "Ġ", "Ĝ", "Ğ", "Ģ", "á", "à", "â", "ä", "ă", "ā", "ã", "å", "ą", "æ", "ć", "ċ", "ĉ", "č", "ç", "ď", "đ", "ð", "é", "è", "ė", "ê", "ë", "ě", "ē", "ę", "ə", "ġ", "ĝ", "ğ", "ģ", "Ĥ", "Ħ", "I", "Í", "Ì", "İ", "Î", "Ï", "Ī", "Į", "Ĳ", "Ĵ", "Ķ", "Ļ", "Ł", "Ń", "Ň", "Ñ", "Ņ", "Ó", "Ò", "Ô", "Ö", "Õ", "Ő", "Ø", "Ơ", "Œ", "ĥ", "ħ", "ı", "í", "ì", "i", "î", "ï", "ī", "į", "ĳ", "ĵ", "ķ", "ļ", "ł", "ń", "ň", "ñ", "ņ", "ó", "ò", "ô", "ö", "õ", "ő", "ø", "ơ", "œ", "Ŕ", "Ř", "Ś", "Ŝ", "Š", "Ş", "Ť", "Ţ", "Þ", "Ú", "Ù", "Û", "Ü", "Ŭ", "Ū", "Ů", "Ų", "Ű", "Ư", "Ŵ", "Ý", "Ŷ", "Ÿ", "Ź", "Ż", "Ž", "ŕ", "ř", "ś", "ŝ", "š", "ş", "ß", "ť", "ţ", "þ", "ú", "ù", "û", "ü", "ŭ", "ū", "ů", "ų", "ű", "ư", "ŵ", "ý", "ŷ", "ÿ", "ź", "ż", "ž");
        $to   = array("A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "C", "C", "C", "C", "C", "D", "D", "D", "E", "E", "E", "E", "E", "E", "E", "E", "G", "G", "G", "G", "G", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "c", "c", "c", "c", "c", "d", "d", "d", "e", "e", "e", "e", "e", "e", "e", "e", "g", "g", "g", "g", "g", "H", "H", "I", "I", "I", "I", "I", "I", "I", "I", "J", "J", "K", "L", "L", "N", "N", "N", "N", "O", "O", "O", "O", "O", "O", "O", "O", "C", "h", "h", "i", "i", "i", "i", "i", "i", "i", "i", "j", "j", "k", "l", "l", "n", "n", "n", "n", "o", "o", "o", "o", "o", "o", "o", "o", "o", "R", "R", "S", "S", "S", "S", "T", "T", "T", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "W", "Y", "Y", "Y", "Z", "Z", "Z", "r", "r", "s", "s", "s", "s", "B", "t", "t", "b", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "w", "y", "y", "y", "z", "z", "z");

        $symbolFrom = array(" ");
        $symbolTo = array("_");

        $from = array_merge($from, $cyrylicFrom, $symbolFrom);
        $to   = array_merge($to, $cyrylicTo, $symbolTo);

        $newstring=str_replace($from, $to, $string); 

        return $newstring;
    }
    
    function getStaffProcedureList($iStaffId = 0, $sFieldsForSelect = '') 
    {
        $oSqlObject = new ui_clasess_Sql();
        
        if($iStaffId == 0)
        {
            return array();
        }
        
        if($sFieldsForSelect != '')
        {
            $aFieldsForSelect = explode(',', $sFieldsForSelect);
            foreach($aFieldsForSelect as $sFieldForSelect)
            {
                $oSqlObject->select($sFieldForSelect);
            }
        }
        else
        {
            $oSqlObject->select('proc.*');
        }
                
        $oSqlObject->from('staffposition stfp');
        $oSqlObject->from('procedurespositions procpos');
        $oSqlObject->from('procedures proc');
        $oSqlObject->where('stfp.positionid=procpos.positionsid');
        $oSqlObject->where('procpos.proceduresid=proc.id');
        $oSqlObject->where('staffid="'.$iStaffId.'"');
        

        $sSql = $oSqlObject->getSql();
        
        $old = $this->parser->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $oResult = $this->parser->conn->SelectLimit($sSql);
        $this->parser->conn->SetFetchMode($old);
        
        while($row = $oResult->FetchRow())
        {
            $row['iframeurl'] = '/staff/'.$iStaffId.'/procedures/'.$row['id'];
            $aResult[] = $row;
        }
        
        if(empty($aResult))
        {
            return array();
        }
        
        return $aResult;
    }
}
