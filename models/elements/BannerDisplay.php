<?php
/**
 * Description of DefaultPage
 *
 * @author dmitr
 */
class pageclients_models_elements_BannerDisplay
{
    protected $parser;

    public function __construct($parser)
    {
        $this->parser = $parser;
    }

    function getBannersList($sPageId = '', $sFieldsForSelect = '', $bActive = true, $iListLimit = 0) 
    {

        $oSqlObject = new ui_clasess_Sql();
        
        if($sFieldsForSelect != '')
        {
            $aFieldsForSelect = explode(',', $sFieldsForSelect);
            foreach($aFieldsForSelect as $sFieldForSelect)
            {
                $oSqlObject->select($sFieldForSelect);
            }
        }
        else
        {
            $oSqlObject->select('bnr.*');
        }
        $oSqlObject->select('bnrset.id AS setid');
        
        $oSqlObject->from('pageclientsbannerstable bnr');
        $oSqlObject->from('pageclientsbannerssetstable bnrset');
        
        $oSqlObject->where('bnr.bannersset=bnrset.id');
        
        if($sPageId != '')
        {
            $oSqlObject->where('bnrset.linkedpage="'.$sPageId.'"');
        }
        
        if($bActive)
        {
           $oSqlObject->where('bnr.active=1');
           $oSqlObject->where('bnrset.active=1');
        }
        
        $sSql = $oSqlObject->getSql();

        $old =  $this->parser->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        if($iListLimit != 0)
        {
            $oResult = $this->parser->conn->SelectLimit($sSql, $iListLimit);
        }
        else 
        {
            $oResult = $this->parser->conn->SelectLimit($sSql);
        }
        $this->parser->conn->SetFetchMode($old);
        
        $aResult = array();
        while($oRow = $oResult->FetchRow())
        {
            if(!array_key_exists($oRow['id'], $aResult))
            {
                foreach($oRow as $sRowElementKey => $sRowElementValue)
                {
                    $aTempResult[$sRowElementKey] = $sRowElementValue;
                }
                $aResult[$oRow['setid']][] = $aTempResult;
            }
            else
            {
                foreach($oRow as $sRowElementKey => $sRowElementValue)
                {
                    $aTempResult[$sRowElementKey] = $sRowElementValue;
                }
                array_push($aResult[$oRow['setid']], $aTempResult);
            }
        }
        
        if(empty($aResult))
        {
            return array();
        }
        
        return $aResult;
    }
    
}
