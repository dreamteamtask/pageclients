<?php
/**
 * Description of DefaultPage
 *
 * @author dmitr
 */
class pageclients_models_elements_Menu
{
    protected $parser;

    public function __construct($parser)
    {
        $this->parser = $parser;
    }
    
    function getMenuList($sFieldsForSelect = '', $bPublished = false, $bSorting = true, $iListLimit = 0) 
    {

        $oSqlObject = new ui_clasess_Sql();
        
        if($sFieldsForSelect != '')
        {
            $aFieldsForSelect = explode(',', $sFieldsForSelect);
            foreach($aFieldsForSelect as $sFieldForSelect)
            {
                $oSqlObject->select($sFieldForSelect);
            }
        }
        else
        {
            $oSqlObject->select('*');
        }
        
        $oSqlObject->from('pageclientsmenupagestable');
        
        if($bPublished)
        {
           $oSqlObject->where('published=1');
        }
        
        if($bSorting)
        {
            $oSqlObject->orderby('sortpos');
        }
        
        $sSql = $oSqlObject->getSql();
        
        $old =  $this->parser->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        if($iListLimit != 0)
        {
            $oResult = $this->parser->conn->SelectLimit($sSql, $iListLimit);
        }
        else 
        {
            $oResult = $this->parser->conn->SelectLimit($sSql);
        }
        $this->parser->conn->SetFetchMode($old);
        
        while($row = $oResult->FetchRow())
        {
            if(($row['parent'] == 0) && ($row['isfolder'] == 1))
            {
                $row['subitem'] = $this->getChildCategories(0);
            }
            $aResult[] = $row;
            
        }

        if(empty($aResult))
        {
            return array();
        }
        
        return $aResult;
    }
    
    function getChildCategories($iStartId = 0) 
    {
        $oSqlObject = new ui_clasess_Sql();
        
        $oSqlObject->select('*');
        $oSqlObject->from('categoryprocedures');
        $oSqlObject->where('parent='.$iStartId);
        
        $sSql = $oSqlObject->getSql();
        
        $old =  $this->parser->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $oResult = $this->parser->conn->SelectLimit($sSql);
        $this->parser->conn->SetFetchMode($old);
        
        while($row = $oResult->FetchRow())
        {
            if($row['isfolder'] == 1)
            {
                $row['subitem'] = $this->getChildCategories($row['id']);
            }
            else
            {
                $row['subitem'] = $this->getChildProcedures($row['id']);
            }
            $aResult[] = $row;
        }
        
        return $aResult;
    }
    
    function getChildProcedures($iStartId = 0) 
    {
        $oSqlObject = new ui_clasess_Sql();
        
        $oSqlObject->select('*');
        $oSqlObject->from('procedures');
        $oSqlObject->where('categoryid='.$iStartId);
        
        $sSql = $oSqlObject->getSql();
        
        $old =  $this->parser->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $oResult = $this->parser->conn->SelectLimit($sSql);
        $this->parser->conn->SetFetchMode($old);
        
        while($row = $oResult->FetchRow())
        {
            $aResult[] = $row;
        }
        
        return $aResult;
    }
    
}