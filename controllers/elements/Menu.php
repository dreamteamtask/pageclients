<?php

/**
 * Description of DefaultPage
 *
 * @author dmitr
 */
class pageclients_controllers_elements_Menu extends common_controller
{
    
    function index($param = '') 
    {
        $oMenuModel = new pageclients_models_elements_Menu($this->parser);
        $this->menuList = $oMenuModel->getMenuList();
        $this->render();
    }
}