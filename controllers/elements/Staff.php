<?php

/**
 * Description of DefaultPage
 *
 * @author dmitr
 */
class pageclients_controllers_elements_Staff extends common_controller
{
    
    function index($param = '') 
    {
        $oStaffModel = new pageclients_models_elements_Staff($this->parser);
 
		$this->addChild('pageclients/elements/Menu');
        $this->staffList = $oStaffModel->getStaffListWithPosition();
        $aScheduleArray = explode(',', $this->parser->gs('wcrmp_workdays'));
        $sDaySchedule = $this->parser->gs('wcrmp_daybegin').' - '.$this->parser->gs('wcrmp_dayend');
        $aDaysArrayLongEn = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
        $aDaysArrayShortEn = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
        $aDaysArrayLongRu = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'];
        $aDaysArrayShortRu = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];
        $bShortName = true;
        $aWeekSchedule = '';
        if($bShortName)
        {
            $aWeekSchedule = $aDaysArrayShortRu[trim(array_shift($aScheduleArray))].' - '.$aDaysArrayShortRu[trim(array_pop($aScheduleArray))];
        }
        else
        {
            $aWeekSchedule = $aDaysArrayLongRu[trim(array_shift($aScheduleArray))].' - '.$aDaysArrayLongRu[trim(array_pop($aScheduleArray))];
        }
        $this->weekSchedule = $aWeekSchedule;
        $this->weekend = $weekend;
        $this->daySchedule = $sDaySchedule;
        $this->render();
    }
}