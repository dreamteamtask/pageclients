<?php

/**
 * Description of DefaultPage
 *
 * @author dmitr
 */
class pageclients_controllers_elements_ServicesIndex extends common_controller
{
    
    function index($param = '') 
    {
        $oServicesModel = new pageclients_models_elements_Services($this->parser);
        $this->servicesListRandom = $oServicesModel->getServices(0, '', '', false, 4, true);
        $this->render();
    }
}