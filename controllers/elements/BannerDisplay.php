<?php

/**
 * Description of DefaultPage
 *
 * @author dmitr
 */
class pageclients_controllers_elements_BannerDisplay extends common_controller
{
    
    function index($param = '') 
    {
        $oBannerDisplayModel = new pageclients_models_elements_BannerDisplay($this->parser);
        $this->bannersList = $oBannerDisplayModel->getBannersList($param);
        $this->render();
    }
}