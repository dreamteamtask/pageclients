<?php

/**
 * Description of DefaultPage
 *
 * @author dmitr
 */
class pageclients_controllers_elements_StaffIndex extends common_controller
{
    
    function index($param = '') 
    {
        $oStaffModel = new pageclients_models_elements_Staff($this->parser);
        $this->staffList = $oStaffModel->getStaffListWithPosition(0, false, false, 2, true);
        $this->render();
    }
}