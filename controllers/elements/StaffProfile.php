<?php

/**
 * Description of DefaultPage
 *
 * @author dmitr
 */
class pageclients_controllers_elements_StaffProfile extends common_controller
{
    
    function index($param = '') 
    {
        $iStaffId = ($this->request['staffid']) ? $this->request['staffid'] : 0;
        $oStaffModel = new pageclients_models_elements_Staff($this->parser);
        $this->staffProfile = $oStaffModel->getStaffListWithPosition($iStaffId);
        $this->staffProcedureList = $oStaffModel->getStaffProcedureList($iStaffId);
        $this->staffRandomList = $oStaffModel->getStaffListWithPosition(0, false, false, 4, true, [$iStaffId]);
        $this->staffId = $iStaffId;
        $this->addChild('pageclients/elements/Menu');
        $aScheduleArray = explode(',', $this->parser->gs('wcrmp_workdays'));
        $sDaySchedule = $this->parser->gs('wcrmp_daybegin').' - '.$this->parser->gs('wcrmp_dayend');
        $aDaysArrayLongEn = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
        $aDaysArrayShortEn = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
        $aDaysArrayLongRu = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'];
        $aDaysArrayShortRu = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];
        $bShortName = true;
        $aWeekSchedule = '';
        if($bShortName)
        {
            $aWeekSchedule = $aDaysArrayShortRu[trim(array_shift($aScheduleArray))].' - '.$aDaysArrayShortRu[trim(array_pop($aScheduleArray))];
        }
        else
        {
            $aWeekSchedule = $aDaysArrayLongRu[trim(array_shift($aScheduleArray))].' - '.$aDaysArrayLongRu[trim(array_pop($aScheduleArray))];
        }
        $this->weekSchedule = $aWeekSchedule;
        $this->weekend = $weekend;
        $this->daySchedule = $sDaySchedule;
        $this->render();
    }
}