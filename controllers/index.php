<?php

/**
 * Description of DefaultPage
 *
 * @author dmitr
 */
class pageclients_controllers_index extends common_controller
{
    
    function index($param = '')
    {

        $this->addChild('pageclients/elements/StaffIndex');
        $this->addChild('pageclients/elements/Rewiews');
        $this->addChild('pageclients/elements/ServicesIndex');
        $this->addChild('pageclients/elements/Gallery');
        $this->addChild('pageclients/elements/Menu');
        $this->addChild(array('pageclients/elements/BannerDisplay', $this->parser->pageId));
        $this->render();
        
    }
    
}
