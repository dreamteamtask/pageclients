<!DOCTYPE html>
<html>
<head>
	<title><?php print($metatitle) ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=<?php print($charset) ?>">
	<meta name="keywords" content="<?php print($metakeywords) ?>">
	<meta name="description" content="<?php print($metadescription) ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<base href="<?php print($basurl) ?>">
	<meta name="theme-color" content="#34495E">
	<meta name="msapplication-navbutton-color" content="#34495E"><!-- Windows Phone -->
	<meta name="apple-mobile-web-app-capable" content="yes"><!-- iOS Safari -->
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"><!-- iOS Safari -->
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

	<link href="<?php print(SYS_BASE_URL) ?>ext/<?php print($actual_viewmodule) ?>/views/theme/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php print(SYS_BASE_URL) ?>ext/<?php print($actual_viewmodule) ?>/views/theme/css/bootstrap-slider.css" rel="stylesheet">
	<link href='<?php print(SYS_BASE_URL) ?>ext/<?php print($actual_viewmodule) ?>/views/theme/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
	<link href="<?php print(SYS_BASE_URL) ?>ext/<?php print($actual_viewmodule) ?>/views/theme/css/style.css" rel="stylesheet">
	<link href="<?php print(SYS_BASE_URL) ?>ext/<?php print($actual_viewmodule) ?>/views/theme/css/media.css" rel="stylesheet">
	<link href='https://fonts.googleapis.com/css?family=Playfair+Display:400,700' rel='stylesheet' type='text/css'>
	
	<script src="<?php print(SYS_BASE_URL) ?>ext/<?php print($actual_viewmodule) ?>/views/theme/js/jquery-2.2.0.min.js" type="text/javascript"></script>
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
  	<script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  	<script type='text/javascript' src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
  	<![endif]-->
  </head>
  <body>
  	<div class="containerFirst">
  		<div class="headerMini">
  			<div class="container">
  				<div class="row">
  					<div class="col-md-3 col-lg-3 leftColumn">
  						<ul class="social">
  							<li><a href=""><i class="fa fa-facebook"></i></a></li>
  							<li><a href=""><i class="fa fa-twitter"></i></a></li>
  							<li><a href=""><i class="fa fa-google-plus"></i></a></li>
  						</ul>
  						<ul class="login">
  							<li><a href="/ui/login"><span>Login</span></a></li>
  						</ul>
  					</div>
  					<div class="col-md-9 col-lg-9 rightColumn">
  						<ul class="cart hidden">
  							<li><a href="">
  								<i class="fa fa-shopping-cart"></i>
  								<p>Login Your Cart: 1 item - $70.00</p>
  							</a></li>
  						</ul>
  						<ul class="searsh hidden">
  							<li>
  								<a href="#">
  									<i class="fa fa-search"></i>
  									<input type="text" placeholder="Search" class="headerSearch">
  									<i class="fa fa-times"></i>
  								</a>
  							</li>
  						</ul>
  					</div>
  				</div>
  			</div>
  		</div>
  		<div class="header">
  			<div class="container">
  				<div class="row">
  					<div class="col-md-3 col-lg-3 work">
  						<p>Mon - Sat 8.00 - 20.00</p>
  						<span>Sunday CLOSED</span>
  						<i class="fa fa-clock-o"></i>
  					</div>
  					<div class="col-md-6 col-lg-6 logo">
  						<a href="/">
  							<img src="<?php print(SYS_BASE_URL) ?>ext/<?php print($actual_viewmodule) ?>/views/theme/images/logotype.svg">
  						</a>
  					</div>
  					<div class="col-md-3 col-lg-3 phone">
  						<i class="fa fa-phone"></i>
  						<p>Звоните нам: <?php print($parser->gs('phoneHeader')); ?></p>
  						<span><?php print($parser->gs('sys_vars_site_email')); ?></span>
  					</div>
  				</div>
  			</div>
  		</div>
  		<div class="backgroundMenu">
  			<div class="container">
  				<div class="row">
  					<div>
  						<?php print $Menu; ?>
  					</div>
  				</div>
  			</div>
  		</div>

  		<?php print $content; ?>

  	</div>

  	<p class="scrollUp"><i class="fa fa-angle-up"></i></p>
  	<footer>
  		<div class="container">
  			<div class="row">
  				<div class="col-lg-3 col-md-3 col-sm-6 loginFooter">
  					<img src="<?php print(SYS_BASE_URL) ?>ext/<?php print($actual_viewmodule) ?>/views/theme/images/logotype.svg">
  					<p>We are a warm and friendly center with high beauty &amp; health standards. We are the best at what we do.</p>
  					<ul>
  						<li><a href=""><i class="fa fa-facebook"></i></a></li>
  						<li><a href=""><i class="fa fa-twitter"></i></a></li>
  						<li><a href=""><i class="fa fa-google-plus"></i></a></li>
  					</ul>
  				</div>
  				<div class="col-lg-3 col-md-3 col-sm-6">
  					<p class="footerCaptDiv">Recent Comments</p>
  				</div>
  				<div class="col-lg-3 col-md-3 col-sm-6">
  					<p class="footerCaptDiv">TWITTER</p>
  				</div>
  				<div class="col-lg-3 col-md-3 col-sm-6">
  					<p class="footerCaptDiv">RECENT POSTS</p>
  				</div>
  			</div>
  		</div>
  	</footer>
  	<div class="footerAfter">
  		<div class="container">
  			<div class="row">
  				BBcloud © 2016 All Rights Reserved
  			</div>
  		</div>
  	</div>

  	<script defer src="<?php print(SYS_BASE_URL) ?>ext/<?php print($actual_viewmodule) ?>/views/theme/js/bootstrap.min.js" type="text/javascript"></script>
  	<script defer src="<?php print(SYS_BASE_URL) ?>ext/<?php print($actual_viewmodule) ?>/views/theme/js/bootstrap-slider.js" type="text/javascript"></script>
  	<script defer src="<?php print(SYS_BASE_URL) ?>ext/<?php print($actual_viewmodule) ?>/views/theme/js/common.js"></script>
  	<script type="text/javascript" src="<?php print(SYS_BASE_URL) ?>ext/<?php print($actual_viewmodule) ?>/views/theme/js/jquery.nivo.slider.js"></script> 
  	<script type="text/javascript">
  		$(window).load(function() {
  			$('#slider').nivoSlider({
  				effect: 'random',
  				slices: 15,
  				boxCols: 8,
  				boxRows: 4,
  				animSpeed: 500,
  				pauseTime: 5000,
  				startSlide: 0,
  				directionNav: true,
  				controlNav: true,
  				controlNavThumbs: false,
  				pauseOnHover: true,
  				manualAdvance: false,
  				prevText: '<i class="fa fa-angle-left"></i>',
  				nextText: '<i class="fa fa-angle-right"></i>',
  				randomStart: false,
  				beforeChange: function(){},
  				afterChange: function(){},
  				slideshowEnd: function(){},
  				lastSlide: function(){},
  				afterLoad: function(){}
  			});
  		});
  		$(function () {
  			$('[data-toggle="tooltip"]').tooltip()
  		});
  		
  	</script>
  </body>
  </html>