<div class="container">
  <div class="row">
    <div class="pProcedureParent">
      <h2 class="staffBigCapt">Разделы</h2>

      <?php //print_r($servicesCategoriesList)?>

      <?php foreach ($servicesCategoriesList as $serv) { ?>
      <div class="col-md-4 col-lg-4">
      <div class="divSpPrice">
          <div class="pProcedureImg">
            <a href="<?php print($serv['shorturl']) ?>" class="imgLinkCategory">
                <?php if($serv['avatar'] == '') {?>
              <img src="/ext/pageclients/views/theme/images/noimage.png">
                <?php } else {?>
                <img src="<?php print($serv['avatar']) ?>" class="img_Sp">
                <?php }?>
            </a>
          </div>
          <p class="pProcedureCaption"><?php print($serv['title']) ?></p>
          <span class="pProcedureMini">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempornt, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </span>
          <a href="<?php print($serv['shorturl']) ?>" class="pProcedurelink">Перейти к разделу</a>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>
</div>