<?php //print_r($staffProfile)?>
<div class="container">
    <div class="row">

        <div class="BreadcrumbList">
            <a class="home" href="http://beautyou.axiomthemes.com">Home</a>
            <span class="breadcrumbs_delimiter"> / </span>
            <a class="all" href="http://beautyou.axiomthemes.com/blog-streampage/">All Posts</a>
            <span class="breadcrumbs_delimiter"> / </span><span class="current">Amanda Smith</span>
        </div>

        <div class="staffProfile col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <?php foreach ($staffProfile as $firsStaff) { ?>
            <h1><?php print($firsStaff['fullname']) ?></h1>
            <div class="col-lg-4 col-md-4">
                <img src="<?php print($firsStaff['avatar']) ?>" class="profileStafImg">
            </div>
            <div class="col-lg-4 col-md-4">
                <p class="captionPrInfo">About Me</p>
                <span><?php print($firsStaff['notes']) ?></span>
                <ul class="stProfileSocial">
                    <li><a href=""><i class="fa fa-facebook"></i></a></li>
                    <li><a href=""><i class="fa fa-twitter"></i></a></li>
                    <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                </ul>
            </div>
            <div class="col-lg-4 col-md-4 informationStaff">
                <p>Мої дані</p>
                <span><b>Адреса:</b> <?php print($firsStaff['city']) ?>, <?php print($firsStaff['address']) ?></span>
                <span><b>Телефон:</b> <?php print($firsStaff['phone']) ?></span>
                <span><b>E-mail:</b> <?php print($firsStaff['email']) ?></span>
                <span><b>День рождения:</b> <?php print($firsStaff['datebirth']) ?></span>
                <?php foreach ($firsStaff['positions'] as $val) {
                    if ($val['main']) {
                        ?>
                        <span><b>Должность:</b> <?php print($val['title']) ?></span>
                    <?php } else { ?>

                    <?php }
                } ?>
                <a href="#" class="toProcedure" data-toggle="modal" data-target="#staffProcedure"><span class="preloadBtn"></span><span class="preloadBtnHide">Записаться на приём</span></a>
            </div>
        </div>
    </div>
</div>

<div class="backgroundPrInfoStaff">
    <div class="container">
        <div class="row">
            <p class="staffBigCapt">Процедуры которые я делаю</p>

            <?php foreach ($staffProcedureList as $kill) { ?>

                <div class="col-lg-3 col-md-3 divSpPrice">
                    <p class="priceCaptSp"><?php print($kill['title']) ?></p>
                    <div class="hoverPriceSp">
                        <img src="<?php print($kill['avatar']) ?>" class="img_Sp">
                    </div>
                    <b class="myProcedureB">Цена:</b><span class="myProcedure"> <?php print($kill['price']) ?></span>
                    <b class="myProcedureB">Длительность процедуры:</b><span
                        class="myProcedure"> <?php print($kill['duration']) ?></span>
                    <a href="<?php print($kill['shorturl']) ?>">Ссылка на процедуру</a>
                    <a href="#" data-toggle="modal" data-target="#staffProcedure"
                       onclick="$('#staffProcedure').find('iframe').attr('src', 'http://developer.bbcloud.online/salon/record/iframe/<?php print((($staffId) ? 'staff/' . $staffId : '')); ?>' + '/procedures/' + '<?php print($kill['id']); ?>');">Записаться</a>
                </div>

            <?php } ?>

        </div>

        <?php } ?>

    </div>
</div>

<?php //print_r($staffRandomList)?>


<div class="backgroundPrInfoStaff profileOtherStaff">
    <div class="container">
        <div class="row">
            <div class="pProcedureParent">
                <p class="staffBigCapt">Другие наши сотрудники</p>

                <?php foreach ($staffRandomList as $staff) { ?>

                    <div class="col-md-3 col-lg-3 divSpPrice">
                        <div class="hoverPriceSp">
                            <img class="img_Sp" alt="t_1.jpg" src="<?php print($staff['avatar']) ?>">
                        </div>
                        <div class="staffPage">
                            <a class="" href="<?php print($staff['shorturl']) ?>"><i class="fa fa-male"
                                                                                     data-toggle="tooltip"
                                                                                     data-placement="top"
                                                                                     title="Перейти"></i></a>
                        </div>
                        <div class="staffCapt">
                            <p><?php print($staff['firstname']) ?><?php print($staff['lastname']) ?></p>
                            <?php foreach ($staff['positions'] as $position) {
                                if ($position['main'] == 1) { ?>
                                    <b><span><?php print($position['title']) ?></span></b><br>
                                <?php } else { ?>
                                    <span><?php print($position['title']) ?></span><br>
                                <?php }
                            } ?>
                        </div>
                    </div>

                <?php } ?>

            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="staffProcedure" data-procedureid="" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <p>Запись на процедуру</p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">Закрыть</button>
            </div>
            <div class="modal-body" data-baseurl="http://developer.bbcloud.online/salon/record/iframe/">
                <iframe src=""></iframe>
            </div>
        </div>
    </div>
</div>