<div class="sc_section">
	<h2 class="staffBigCapt">Наши сотрудники</h2>
    <div class="staffMiniCapt"> Beautyou is a warm and friendly beauty &amp; spa center whose team<br>
        has high beauty and health standards. We are the best at what we do. </div>

        <?php foreach($staffList as $staff){  ?>
        <div class="col-md-3 col-lg-3">
            <div class="staffParent">
              <div class="staff">

               <img class="wp-post-image" alt="t_1.jpg" src="<?php print($staff['avatar']) ?>">
                           <!--  <ul class="sc_team_item_socials">
                                <li>
                                   <a href="https://twitter.com/axiomthemes" class="social_icons social_twitter icon-twitter" target="_blank"></a>
                                </li>
                                <li>
                                    <a href="https://plus.google.com/102189073109602153696" class="social_icons social_gplus icon-gplus" target="_blank"></a>
                                </li>
                                <li>
                                    <a href="https://www.facebook.com/" class="social_icons social_facebook icon-facebook" target="_blank"></a>
                                </li>
                            </ul>-->
                        </div>

                        <div class="staffPage">
                        	<a class="" href="<?php print($staff['shorturl']) ?>"><i class="fa fa-male"></i></a>
                        </div>

                        <div class="staffCapt">
                        	<p><?php print($staff['firstname']) ?> <?php print($staff['lastname']) ?></p>
                            <?php foreach($staff['positions'] as $position) { 
                                if($position['main'] == 1) { ?>
                                <b><span><?php print($position['title']) ?></span></b><br>
                                <?php } else { ?>
                                <span><?php print($position['title']) ?></span><br>
                                <?php } }?>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>