<?php //print_r($ServicesProfile)?>

<div class="container">
  <div class="row">

    <div class="BreadcrumbList">
      <a class="home" href="http://beautyou.axiomthemes.com">Home</a>
      <span class="breadcrumbs_delimiter"> / </span>
      <a class="all" href="http://beautyou.axiomthemes.com/blog-streampage/">All Posts</a>
      <span class="breadcrumbs_delimiter"> / </span><span class="current">Amanda Smith</span>
    </div>

    <div class="staffProfile col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="divServProfile">
        <?php foreach($ServicesProfile as $firsCartServices) { ?>

        <h1><?php print($firsCartServices['title']) ?></h1>

        <div class="col-lg-5 col-md-5">
          <img src="<?php print($firsCartServices['avatar']) ?>" class="profileStafImg">
        </div>

        <div class="col-lg-7 col-md-7 informationStaff">
          <p>Информация о процедуре:</p>
          <span><b>Цена:</b> <?php print($firsCartServices['price']) ?></span>
      <span><b>Длительность процедуры:</b> <?php print($firsCartServices['duration']) ?></span>
           <a class="toProcedure" onclick="waitAndLoadIframe(this); "><span class="preloadBtn"></span><span class="preloadBtnHide">Заказать процедуру</span></a>
        </div>

        <?php } ?>
      </div>
    </div>
  </div>
</div>

<div class="backgroundPrInfoStaff">
  <div class="container">
    <div class="row">

      <div class="servisesIndexStaff">
        <?php print $ServicesIndex; ?>
      </div>

      <?php //print_r($staffForProcedure)?>
      <div class="moreWorkStaff">
        <p class="staffBigCapt">Эту процедуру делают еще:</p>
        <div class="staffMiniCapt"> Beautyou is a warm and friendly beauty &amp; spa center whose team<br>
          has high beauty and health standards. We are the best at what we do. </div>

          <?php foreach($staffForProcedure as $kill) { ?>

          <div class="col-lg-3 col-md-3 divSpPrice">
            <p class="priceCaptSp"><?php print($kill['firstname']) ?> <?php print($kill['lastname']) ?></p>
            <div class="hoverPriceSp">
              <img src="<?php print($kill['avatar']) ?>" class="img_Sp">
            </div>
            <b class="myProcedureB">Цена:</b><span class="myProcedure"> <?php if(isset($kill['staffprice'][0]) && ($kill['staffprice'][0] != 0)) { print($kill['staffprice'][0]); } elseif(!isset($kill['defaultprice']) || $kill['defaultprice'] == 0) { print('No Price'); } else { print($kill['defaultprice']); }  ?></span>
            <b class="myProcedureB">Длительность процедуры:</b><span class="myProcedure"> <?php print($kill['duration']) ?></span>
            <a href="<?php print($kill['shorturl']) ?>">Ссылка на процедуру</a>
            <a class="toProcedure" onclick="waitAndLoadIframe(this); "><span class="preloadBtn"></span><span class="preloadBtnHide">Записаться на приём</span></a>
          </div>
<!--$('#staffProcedure').find('iframe').attr('src', 'http://developer.bbcloud.online/salon/record/iframe/<?php print((($staffId) ? 'staff/'.$staffId : '')); ?>' + '/procedures/' + '<?php print($kill['id']); ?>');-->
          <?php } ?>
        </div>

      </div>
    </div>
  </div>

  <div class="modal fade" id="staffProcedure" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <p>Запись на процедуру</p>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
        </div>
        <div id="ifrholder" class="modal-body" data-baseurl="http://developer.bbcloud.online/salon/record/iframe<?php print((($staffId) ? '/staff/'.$staffId : '').(($serviceId) ? '/procedures/'.$serviceId : '')); ?>">
          <iframe id="ifr" src=""></iframe>
        </div>
      </div>
    </div>
  </div>

<script>
    function waitAndLoadIframe(element)
    {
        $(element).find('.preloadBtnHide').slideToggle();
        $(element).find('.preloadBtn').slideToggle();
        setTimeout(function(){
            $('#ifr').attr('src', $('#ifrholder').attr('data-baseurl')); //$(element).attr('data-suburl')
            document.getElementById('ifr').onload = function()
            {
                $('#staffProcedure').modal('show');
                $(element).find('.preloadBtnHide').slideToggle();
                $(element).find('.preloadBtn').slideToggle();
            }
        }, 200);
    }
          
    $(function()
    {
        $(window).on("message", function(e) 
        {
            var data = e.originalEvent.data;
            if (data) 
            {
                var str = 'Пришли неверные данные';
                console.log(data);
                if (data.status || data.mess) 
                {
                    str = 'Сообщение:' + data.status + '. Значение объекта:' + data.mess;
                }
               // alert(str);
                $('#staffProcedure').modal('hide');
            }
        });
    });
</script>