<ul class="menuLink">
<?php foreach($menuList as $menuElement) { ?>
    <li><a class="<?php print((($menuElement['isfolder']) ? 'dropdownlink' : '')); ?>" href="<?php print($menuElement['shorturl']); ?>"><?php print($menuElement['title']); ?></a>
        <?php if ($menuElement['isfolder']) { ?>
            <ul class="dropdownMenuLink">
                <?php foreach($menuElement['subitem'] as $subcategItem) { ?>
                <li>
                    <a href="<?php print($subcategItem['shorturl']); ?>"><?php print($subcategItem['title']); ?></a>
                        <?php if ($subcategItem['subitem']) { ?>
                            <ul class="subDropdownMenuLink">
                                <?php foreach($subcategItem['subitem'] as $subItem) { ?>
                                <li>
                                    <a href="<?php print($subItem['shorturl']); ?>"><?php print($subItem['title']); ?></a>
                                </li>
                                <?php } ?>
                            </ul>
                        <?php } ?>
                </li>
                <?php } ?>
            </ul>
        <?php } ?>
        </li>

<?php } ?>


    </ul>
