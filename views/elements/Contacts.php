<div class="pageContact">
	
	<div class="container">
		<div class="row">

			<div class="col-md-12 col-lg-12 col-sm-12">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d754.9162748124462!2d30.48036617962057!3d50.47414449632443!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0xf859c1db740ecad1!2z0J3QvtGH0Ywg0LrQuNC90L4!5e0!3m2!1suk!2sua!4v1456142390422" width="600" height="450" frameborder="0" allowfullscreen></iframe>
			</div>

		</div>
	</div>

	<div class="howWeFound">
		<div class="container">
			<div class="row">
				<div class="staffBigCapt">Special Gifts</div>
				<div class="col-md-4 col-lg-4 col-sm-12">
					<i class="fa fa-map-marker"></i>
					<p>Наша адреса:</p>
					<span><?php print($parser->gs('adressSite')); ?></span>
				</div>
				<div class="col-md-4 col-lg-4 col-sm-12">
					<i class="fa fa-calendar"></i>
					<p>Контактная информиция:</p>
					<span><?php print($parser->gs('phoneHeader')); ?> <?php print($parser->gs('sys_vars_site_email')); ?></span>
				</div>
				<div class="col-md-4 col-lg-4 col-sm-12">
					<i class="fa fa-clock-o"></i>
					<p>Opening Hourse</p>
					<span>Mon – Sat 8.00 – 20.00 Sunday Closed</span>
				</div>
			</div>
		</div>
	</div>

	<div class="contactQuestion">
		<div class="container">
			<div class="row">
				<div class="staffBigCapt">Напишите нам</div>
				<div class="col-md-12 col-lg-12 col-sm-12">
					<form>
						<div class="contactMaxWidth">
							<input type="text" placeholder="Имя" required>
						</div>
						<div class="contactMaxWidth">
							<input type="text" placeholder="Email" required>
						</div>
							<textarea placeholder="Сообщение" class="contactTextarea"></textarea>
							<input type="submit" value="Отправить" class="contactBtnSubmit">
					</form>
				</div>
			</div>
		</div>
	</div>

</div>