<div class="wePageStaff">
    <div class="container">
        <div class="row">

            <div class="sc_section">
               <p class="staffBigCapt">Наши сотрудники</p>
               <div class="staffMiniCapt"> Beautyou is a warm and friendly beauty &amp; spa center whose team<br>
                has high beauty and health standards. We are the best at what we do. </div>
                <?php   //print_r($staffList);?>

                <?php foreach($staffList as $staff){  ?>
                <div class="col-md-3 col-lg-3">
                    <div class="staffParent">
                      <div class="staff">

                       <img class="wp-post-image" alt="t_1.jpg" src="<?php print($staff['avatar']) ?>">
                           <!--  <ul class="sc_team_item_socials">
                                <li>
                                   <a href="https://twitter.com/axiomthemes" class="social_icons social_twitter icon-twitter" target="_blank"></a>
                                </li>
                                <li>
                                    <a href="https://plus.google.com/102189073109602153696" class="social_icons social_gplus icon-gplus" target="_blank"></a>
                                </li>
                                <li>
                                    <a href="https://www.facebook.com/" class="social_icons social_facebook icon-facebook" target="_blank"></a>
                                </li>
                            </ul>-->
                        </div>

                        <div class="staffPage">
                        	<a class="" href="<?php print($staff['shorturl']) ?>"><i class="fa fa-male"></i></a>
                        </div>

                        <div class="staffCapt">
                        	<p><?php print($staff['firstname']) ?> <?php print($staff['lastname']) ?></p>
                            <?php foreach($staff['positions'] as $position) { 
                                if($position['main'] == 1) { ?>
                                <b><span><?php print($position['title']) ?></span></b><br>
                                <?php } else { ?>
                                <span><?php print($position['title']) ?></span><br>
                                <?php } }?>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>

            </div>
        </div>
    </div>

 <div id="carousel-example-generic" class="staffSlider carousel slide" data-ride="carousel">
    <div class="container"> 
        <div class="row">
            <p class="staffBigCapt">Testimonials</p>
            <span class="stafDopInfaCapt"> Beautyou Spa Salon is the best! From being greeted with friendly, smiling faces to receiving the best</span>
            <div class="carousel-inner" role="listbox">

             <?php 

             $ire=1; foreach($staffList as $staff) {  ?>

             <div class="item <?php if($ire==1){ print'active'; }?> ">

                <div class="col-lg-12 parentSlStaffPag">
                <div class="stImgSl">
                    <a class="" href="<?php print($staff['shorturl']) ?>"><img src="<?php print($staff['avatar']) ?>"></a>
                    </div>
                    <a class="" href="<?php print($staff['shorturl']) ?>"><p class="SlStaffCapt"><?php print($staff['firstname']) ?> <?php print($staff['lastname']) ?></p></a>
                    <p><?php print($staff['notes']) ?></p>
                    <p><?php print($staff['city']) ?>, <?php print($staff['email']) ?></p>
                </div>
            </div>

            <?php ++$ire; } ?>

        </div>
    </div>
</div>

<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true">
        <i class="fa fa-angle-left"></i>
    </span>
</a>
<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true">
        <i class="fa fa-angle-right"></i>
    </span>
</a>
</div>