$(document).ready(function() {

	$(".scrollUp").click(function () {
		$("body, html").animate({
			scrollTop: 0
		}, 900);
		return false;
	});
	$('.searsh .fa-search').click(function(){
		if(!$('.searsh a').hasClass('maxWidth')){
			$('.searsh a').addClass('maxWidth');
			$('.headerSearch').addClass('maxWidthInput');
			$('.searsh .fa-times').addClass('showClose');
		}
	});
	$('.searsh .fa-times').click(function(){
		$('.searsh a').removeClass('maxWidth');
		$(this).removeClass('showClose');
	});

});