<?php print $BannerDisplay; ?>

<div class="specialPrice">
	<div class="container">
		<div class="row">
			<?php print $ServicesIndex; ?>
		</div>
	</div>
</div>

<div class="textMainPage">
	<div class="container">
		<div class="row">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
		</div>
	</div>
</div>

<div class="backgroundStaff">
	<div class="container">
		<div class="row">
			<?php print $StaffIndex; ?>
		</div>
	</div>
</div>

<div class="contact">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-lg-6 col-sm-6">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d754.9162748124462!2d30.48036617962057!3d50.47414449632443!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0xf859c1db740ecad1!2z0J3QvtGH0Ywg0LrQuNC90L4!5e0!3m2!1suk!2sua!4v1456142390422" width="600" height="450" frameborder="0" allowfullscreen></iframe>
			</div>
			<div class="col-md-6 col-lg-6 col-sm-6 contactInformation">
				<h2 class="staffBigCapt">Наши контакты</h2>
				<p>You are in good hands with our team of professional hair stylists and spa specialists. We offer your only the the premium quality service.</p>
				<i class="fa fa-map-marker"></i><span><?php print($parser->gs('adressSite')); ?></span>
				<i class="fa fa-clock-o"></i><span>Monday – Saturday <br>from 8:00 to 20:00<br>Free day – Sunday</span>
				<i class="fa fa-mobile"></i><span><?php print($parser->gs('phoneHeader')); ?></span>
			</div>
		</div>
	</div>
</div>